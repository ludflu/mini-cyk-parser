module Main where

import Cyk
import CykParser
import CykTreeExpander

inputwords = "the cat sat on the mat"

main :: IO ()
main = do rules <- readFile "grammar.txt"
          let grammar = readGrammar $ lines rules
              parse = findParseTree [DT,N,V,P,DT,N] grammar 
              trees = unpackParse (parse !! 0) 
          print trees

