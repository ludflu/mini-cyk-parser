{-# LANGUAGE NoMonomorphismRestriction #-}

import Diagrams.Prelude

import Diagrams.TwoD.Layout.Tree
import Diagrams.Prelude
import Diagrams.Backend.SVG.CmdLine

import Cyk
import CykParser
import CykTreeExpander

trees = unpackParse (parse !! 0) where
        parse = findParseTree [DT,N,V,P,DT,N]

tree = trees !! 1

dig :: ParseTree a -> BTree a
dig (Node pos l r) = BNode pos (dig l) (dig r)
dig (Leaf pos) = BNode pos Empty Empty
dig (Term pos t) = BNode pos Empty (dig t)

Just t = uniqueXLayout 2 2 (dig tree)

example = pad 1.1 . lw 0.05 . centerXY
         $ renderTree
             (\n -> (text (show n)
                     <> roundedRect 3 1.3 0.3 # fc gold)
             )
             (~~) t
         `atop` square 1 # scaleY 12 # translateY (-5)
                         # scaleX 34
                         # lw 0 # fc whitesmoke

main = defaultMain example


