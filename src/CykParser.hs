module CykParser where

import qualified Data.Map as M
import Debug.Trace
import Control.Monad.State
import Data.Maybe

import Data.Traversable
import qualified Data.Foldable as F
import Data.Functor
import Cyk

-- start word index, end word index, part of speech
data Entry a = Entry Integer Integer a | None deriving (Ord, Eq, Show)
data Partition = Partition Integer Integer Integer (Production Pos)
type ParseChart = M.Map (Entry Pos) Bool
type BackPointer =  M.Map (Entry Pos) [(Entry Pos,Entry Pos)]

data ChartState = Chart {
    chart ::  ParseChart,
    backPointer :: BackPointer
} deriving Show

numbered n = zip n [1..]

isTerminal :: Production a -> Bool
isTerminal (Terminal _ _) = True
isTerminal Nonterminal{}  = False

getFirstSymbol :: Production a -> a
getFirstSymbol (Terminal h c1) = c1
getFirstSymbol (Nonterminal h c1 c2) = c1

matchesFirst :: (Eq a) => a -> Production a -> Bool
matchesFirst pos production = (pos == getFirstSymbol production) && isTerminal production

findForPos :: (Eq a) => [Production a] -> a -> [Production a]
findForPos prds pos = filter (matchesFirst pos) prds

makeEntry :: (Production a, Integer) -> Entry a
makeEntry (Terminal h t, position) = Entry position 1 h

makeTerminalEntry :: (Production a , Integer) -> Entry a
makeTerminalEntry (Terminal _ t, position) = Entry position 1 t

getHead :: Production a -> a
getHead (Nonterminal a b c) = a
getHead (Terminal a b)  = a

getPos :: Entry a -> a
getPos (Entry _ _ pos) = pos

getConstituents :: Partition -> (Entry Pos, Entry Pos)
getConstituents (Partition i j k (Nonterminal a b c))  = (Entry j k b, Entry (j+k) (i-k) c)

lkup = flip M.lookup

isComplete :: ParseChart -> Partition -> Bool
isComplete chart part  = let (a,b) = getConstituents part
                             found = (lkup chart a, lkup chart b)
                         in case found of
                            (Just _,Just _) -> True
                            otherwise -> False

appendItems :: Entry Pos -> (Entry Pos,Entry Pos) -> State ChartState ()
appendItems key value = do cs <- get
                           let bp = backPointer cs
                               entries = lkup bp key
                               newvalue = case entries of
                                          Nothing -> [value]
                                          Just es -> value:es
                           put cs{backPointer = M.insert key newvalue bp}

updateBackPointer :: Partition -> State ChartState ()
updateBackPointer part = do cs <- get
                            let cons  = getConstituents part  
                                bp = backPointer cs
                                e = Entry j i (getHead r)
                                Partition i j k r = part
                            appendItems e cons


combine ::  Partition -> State ChartState ()
combine part = do cs <- get
                  let parseChart = chart cs
                      Nonterminal a b c = r 
                      Partition i j k r = part
                      nc = M.insert (Entry j i a) True parseChart
                  when (isComplete parseChart part) (do put cs{chart = nc}; updateBackPointer part )

cykBaseCase :: (Eq a, Enum t, Num t) => [Production a] -> ((Production a, t) -> b) -> [a] -> [b]
cykBaseCase grmr entryMaker pos = let term  = map (findForPos (terminalProductions grmr)) pos 
                                      found = filter (\(prds,indx) -> not (null prds))  (numbered term)
                                      expandFoundRules (ls,i) = zip ls (repeat i)
                                      found' = concatMap expandFoundRules found
                                      terminalProductions = filter isTerminal
                                   in map entryMaker found' 

cykRecCase :: [Production Pos] -> [Pos] -> [ Partition ]
cykRecCase grammar input = [ Partition i j k r |
                              i <- [ 2.. n ],                --phrase length (only considers phrases of length 2 because the base case already found the ones of length 1 i.e. the terminals
                              j <- [ 1.. (n - i + 1) ],      --start
                              k <- [ 1.. (i - 1) ],          --partition
                              r <- grammar                    --rule
                            ]
                          where n = fromIntegral $ length input

initChart :: [Production Pos] -> [Pos] -> ChartState 
initChart g i = let baseNonTerminals = cykBaseCase g makeEntry i
                    baseTerminals = cykBaseCase g makeTerminalEntry i
                    backPointerSeed = zipWith (\enonterm eterm -> (enonterm, [(eterm,None)])) baseNonTerminals baseTerminals
                    ct = M.fromList (zip baseNonTerminals  (repeat True) )
                 in Chart{chart = ct, backPointer = M.fromList backPointerSeed}

completeParts :: [Partition] -> State ChartState ChartState
completeParts ps = do forM_ ps combine
                      get

findCharts :: [Production Pos] -> [Pos] -> ChartState          
findCharts g i = evalState  (completeParts (cykRecCase grammar i)) $ initChart g i where
                  grammar = filter (not . isTerminal) g

--given a list of chart entries, finds the items which span the entire input
findS :: [a] -> [Entry a] -> [Entry a]
findS i = filter spansS where 
           spansS (Entry start end r) = start == 1 && end == fromIntegral ( length i )

buildParseTree :: BackPointer -> Entry Pos -> ParseTree Pos
buildParseTree _ None = Leaf Nonce
buildParseTree bp entry = let (Entry start end pos) = entry
                              bpl = M.lookup entry bp
                                    in case bpl of
                                       Just ( ( Entry _ _ lpos ,None):[] ) -> Term pos (Leaf lpos)
                                       Just ( (a,b):[] ) -> Node pos (buildParseTree bp a) (buildParseTree bp b) 
                                       Just es -> MultiNode pos $ mkNode bp entry es
                                       Nothing -> Leaf pos

mkNode :: BackPointer -> Entry Pos -> [(Entry Pos, Entry Pos)] -> [ParseTree Pos]
mkNode bp entry [] = []
mkNode bp entry ((a,b):es) = Node (getPos entry) (buildParseTree bp a) (buildParseTree bp b) : mkNode bp entry es

readGrammar :: [String] -> [Production Pos]
readGrammar lines = map read lines

findParseTree :: [Pos] -> [Production Pos] -> [ParseTree Pos]
findParseTree i g = let ct = findCharts g i
                        ss = findS i (M.keys ( chart ct ) ) 
                     in map (buildParseTree $ backPointer ct) ss

