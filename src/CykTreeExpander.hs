{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE DataKinds #-}

{-# Language TemplateHaskell #-}

module CykTreeExpander where

import qualified Data.Map as M
import Debug.Trace
import Control.Monad.State
import Data.Maybe

import Data.Traversable 
import qualified Data.Foldable as F
import Data.Functor
import Control.Lens
import Control.Lens.Fold
import Control.Applicative

import Cyk
import Control.Monad.State as MS (sequence)


isMulti :: ParseTree a -> Bool
isMulti (MultiNode a lst) = True
isMulti _ = False

-- Compute a list of elements of a container satisfying a predicate.
filterF :: F.Foldable f => (a -> Bool) -> f a -> [a]
filterF p = F.foldMap (\a -> [a | p a] )

makeLenses ''ParseTree

--left :: ParseTree a -> ParseTree a
--left  (Node a left right ) = left
--right (Node a left right ) = right

--multinodes :: Applicative f => (ParseTree a -> f (ParseTree a)) -> ParseTree a -> (f (ParseTree a))
multinodes :: Traversal' (ParseTree a) (ParseTree a)

-- No multinodes here, so we use `pure` so as to inject nothing at all
multinodes inj l@Term{} = pure l
multinodes inj l@Leaf{} = pure l

-- Here's a multinode, so we'll visit it with the injection function
multinodes inj m@MultiNode{} = inj m

-- And we need to neither reject (thus stopping the traversal)
-- nor inject a branch. Instead, we continue the traversal deeper
multinodes inj (Node a l r) = 
        Node a  <$> multinodes inj l
        <*> multinodes inj r

left :: Traversal' (ParseTree a) (ParseTree a)
left inj l@Leaf{} = pure l
left inj (Node a l r) = inj l

right :: Traversal' (ParseTree a) (ParseTree a)
right inj l@Leaf{} = pure l
right inj (Node a l r) = inj r

getOptions (MultiNode pos list) = list

replacements :: ParseTree a -> [[ParseTree a]]
replacements t = MS.sequence $ map getOptions  (t ^.. multinodes)

updateMultiNode' :: ParseTree a -> ParseTree a -> ParseTree a
updateMultiNode' target replacement = (elementOf multinodes 0 .~ replacement) target

updateMultiNode :: ParseTree a -> [ParseTree a] -> ParseTree a
updateMultiNode = foldl updateMultiNode' 

unpackParse :: ParseTree a -> [ParseTree a]
unpackParse tree = map (updateMultiNode tree) (replacements tree)

