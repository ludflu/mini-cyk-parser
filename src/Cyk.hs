{-# LANGUAGE DeriveFunctor #-}
{-# LANGUAGE DeriveFoldable #-}
{-# LANGUAGE DeriveTraversable #-}
{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}

module Cyk where

import Data.Traversable
import qualified Data.Foldable as F
import Data.Functor
import Data.Monoid

--parts of speech
data Pos = ADJ | DT | DP | N | NP | VP | V | PP | P | S | Nonce deriving (Show, Read, Eq, Ord)

-- context free grammar rules in CNF
data Production a = Terminal a a | Nonterminal a a a deriving (Show, Read, Ord,Eq )

data ParseTree a = Node a (ParseTree a) (ParseTree a) | Leaf a  | Term a (ParseTree a) | MultiNode a [ParseTree a] 
        deriving (Show, Functor, F.Foldable, Traversable, Eq)

