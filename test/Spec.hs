{-# LANGUAGE TemplateHaskell #-}

import Cyk
import CykTreeExpander
import CykParser

import Test.QuickCheck
import Test.QuickCheck.All

--ensure we find the ambiguity in the parse below
--testparse = Node S (Node NP (Term DP (Leaf DT)) (Term NP (Leaf N))) (MultiNode VP [Node VP (Node VP (Term VP (Leaf V)) (Term PP (Leaf P))) (Node NP (Term DP (Leaf DT)) (Term NP (Leaf N))),Node VP (Term VP (Leaf V)) (Node PP (Term PP (Leaf P)) (Node NP (Term DP (Leaf DT)) (Term NP (Leaf N))))])
grammar = [
  Nonterminal S NP VP,
  Nonterminal NP DP NP,
  Nonterminal VP VP NP,
  Nonterminal VP VP PP,
  Nonterminal PP PP NP,
  Terminal DP DT,
  Terminal PP P,
  Terminal NP N,
  Terminal VP V
  ]

testparse = (findParseTree [DT,N,V,P,DT,N] grammar )  !! 0
prop_findTwoMultinodes :: Bool
prop_findTwoMultinodes = (length $ replacements testparse) == 2

--ensure we can't find any multinodes after running unpackParse
parse = findParseTree [DT,N,V,P,DT,N] grammar
trees = unpackParse (parse !! 0)

prop_replaceAllMulti :: Bool
prop_replaceAllMulti = (map length $ concatMap replacements trees) == [0,0]

runTests :: IO Bool
runTests = $quickCheckAll

--a parse with multiple MultiNode will be expanded to the cartesian product of the options
multimulti = Node S (MultiNode NP [Node NP (Leaf DT) (Leaf N), Node NP (Leaf ADJ) (Leaf N)]) (MultiNode VP [Leaf V, Leaf N])

prop_updateMultiNode_only_updates_first = (updateMultiNode' multimulti (Leaf Nonce) ) == Node S (Leaf Nonce) (MultiNode VP [Leaf V,Leaf N])

--if there are 2 multinodes with 2 options each, there will be 4 possible parses
prop_expand_multimulti_cartesian_product_len_4  = length (unpackParse multimulti) == 4
prop_expand_multimulti_cartesian_product = (unpackParse multimulti) == [Node S (Node NP (Leaf DT) (Leaf N)) (Leaf V),Node S (Node NP (Leaf DT) (Leaf N)) (Leaf N),Node S (Node NP (Leaf ADJ) (Leaf N)) (Leaf V),Node S (Node NP (Leaf ADJ) (Leaf N)) (Leaf N)]

main :: IO ()
main = runTests >>= \passed -> if passed then putStrLn "All tests passed."
              else putStrLn "Some tests failed."
