mini-cyk-parser
===============

a toy [Cocke–Younger–Kasami](http://en.wikipedia.org/wiki/CYK_algorithm) parser

When the mini-cyk-parser grows up, it wants to become a probablistic parser, ranking the parses in order of probability
(but it should remain true to its purely functional roots)

Right now we can perform a full parse that results in a packed forest of parse trees. When an ambiguous sequence is parsed, we create a multinode that has a list of the possible parses for the sequence. But in order to enumerate the full parses, we need to unpack the parse forest:

1. Find the list of all the lists from all the multinodes
2. Take the [cartesian product](http://en.wikipedia.org/wiki/Cartesian_product) of the list of lists
3. For each element in the resulting tuples, we will replace each multinode with its respective sub-element emitted by the cartesian product.
4. Once we have the fully enumerated parse trees, we'll (eventually) compute their probability and rank them

TODO:

- integrate a parts-of-speech tagger
- attach the input words themselves to the parse, instead of stopping at terminals
- add probabilities so we can rank the parses


## To build and run the parser:

- install the [Haskell Tool Stack](https://docs.haskellstack.org/en/stable/README/)
- `stack build`
- `stack run`

```haskell
import Cyk 
import CykParser
import CykTreeExpander

inputwords = "the cat sat on the mat"

main = do rules <- readFile "grammar.txt"
          let grammar = readGrammar $ lines rules
              parse = findParseTree [DT,N,V,P,DT,N] grammar 
              trees = unpackParse (parse !! 0)  
          print trees
```

Which will emit all the possible parses for the sentence:

[Node S (Node NP (Term DP (Leaf DT)) (Term NP (Leaf N))) (Node VP (Node VP (Term VP (Leaf V)) 
(Term PP (Leaf P))) (Node NP (Term DP (Leaf DT)) (Term NP (Leaf N)))),

Node S (Node NP (Term DP (Leaf DT)) (Term NP (Leaf N))) (Node VP (Term VP (Leaf V)) (Node PP 
(Term PP (Leaf P)) (Node NP (Term DP (Leaf DT)) (Term NP (Leaf N)))))]

I've used the excellent [diagrams library](http://projects.haskell.org/diagrams/) to produce sentence diagrams and illustrate two possible parses.

![Diagram1](http://ludflu.github.io/mini-cyk-parser/diagram1.svg)
![Diagram2](http://ludflu.github.io/mini-cyk-parser/diagram2.svg)

In this case, the ambiguity concerns prepositional phrase attachment. Does the preposition "on" attach to "the mat" or "sat"? I'm not a linguist, but I would test this by removing the noun phrase, and seeing if the verb attachment still makes sense and seems fluent. This leaves the sentence:

###  The cat sat on.

Which seems gramatical, if unlikely. It changes the meaning of the sentence to something like "the cat continued to sit". Now these words tell a story of a meditative cat doing yoga for a long time. So for that reason, I'd say the preposition attaches to the noun phrase "the mat" rather than the verb "sat.

